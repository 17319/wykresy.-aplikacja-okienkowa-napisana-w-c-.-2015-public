﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace dd_10_wykres_funkcja_kwadratowa
{
    public partial class OknoGlowne : Form
    {
        Double xMin = -10;
        Double xMax = 10;
        Double xSkok = .05;
        public OknoGlowne()
        {
            InitializeComponent();
            chartWykres.ChartAreas.First().AxisX.Minimum = xMin;
            chartWykres.ChartAreas.First().AxisX.Maximum = xMax;

            chartWykres.ChartAreas.First().AxisX.Crossing = 0.0;
            chartWykres.ChartAreas.First().AxisX.ArrowStyle = System.Windows.Forms.DataVisualization.Charting.AxisArrowStyle.SharpTriangle;
            chartWykres.ChartAreas.First().AxisX.MajorGrid.Enabled = false;

            chartWykres.ChartAreas.First().AxisY.Crossing = 0.0;
            chartWykres.ChartAreas.First().AxisY.ArrowStyle = System.Windows.Forms.DataVisualization.Charting.AxisArrowStyle.SharpTriangle;
            chartWykres.ChartAreas.First().AxisY.MajorGrid.Enabled = false;

            //buttonRysuj_Click(null, null);
            buttonReset_Click(null, null);

        }

        private void buttonRysuj_Click(object sender, EventArgs e)
        {
            try
            {
            Double a, b, c;
            
            if (Double.TryParse(textBoxA.Text, out a) && a != 0.0 &&
                Double.TryParse(textBoxB.Text, out b) &&
                Double.TryParse(textBoxC.Text, out c))
            {
                Funkcja f = new FunkcjaKwadratowa(a, b, c);
                Series seria = new Series(f.dajNazwe());
                seria.ChartType = SeriesChartType.Line;
                for (double x = xMin; x <= xMax; x += xSkok)
                {
                    seria.Points.Add(new DataPoint(x, f.dajWartosc(x)));
                }
                foreach (Double x in f.dajMiejscaZerowe())
                {
                    DataPoint p = new DataPoint(x, 0);
                    p.MarkerStyle = MarkerStyle.Circle;
                    p.MarkerSize = 8;
                    p.MarkerColor = Color.Red;
                    p.Label = "x=" + Math.Round(x, 2);
                    seria.Points.Add(p);
                }
                seria.Sort(PointSortOrder.Ascending, "X");
                chartWykres.Series.Add(seria);
                chartWykres.ResetAutoValues();
            }
            else
            {
                MessageBox.Show("Nie wprowadzono wartości bądź wprowadzona wartość jest błędna.");
            }
            }
            catch (ArgumentException )
            {

                MessageBox.Show(" Wprowadzona funkcja już znajduje się na wykresie.");
            }
            catch (Exception )
            {
                MessageBox.Show("Wystąpił nieokreślony błąd");
            }
            
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            chartWykres.Series.Clear();
        }
    }
}
