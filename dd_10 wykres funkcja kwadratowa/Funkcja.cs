﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dd_10_wykres_funkcja_kwadratowa
{
    interface Funkcja
    {
        String dajNazwe();
        Double dajWartosc(Double x);
        List<Double> dajMiejscaZerowe();
    }

    class FunkcjaKwadratowa : Funkcja
    {
        Double a, b, c;
        List<Double> miejscaZerowe;
        public FunkcjaKwadratowa(Double a, Double b, Double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;

            Double d = Math.Pow(b, 2) - 4 * a * c;
            miejscaZerowe = new List<double>();
            if (d >= 0)
            {
                miejscaZerowe.Add((-b - Math.Sqrt(d)) / (2 * a));
                if (d!=0)
                miejscaZerowe.Add((-b + Math.Sqrt(d)) / (2 * a));
            }
        }

        public Double dajWartosc(Double x)
        {
            return a * x * x + b * x + c;
        }

        public String dajNazwe()
        {
            String wzor = a.ToString() + "x^2";
            if (b>0)
            {
                wzor += "+" + b.ToString() + "x";
            }
            else if (b < 0)
            {
                wzor += b.ToString() + "x";
            }
            else if (c>0)
            {
                wzor += "+" + c.ToString();
            }
            else if (c<0)
            {
                wzor += c.ToString();
            }
            //return wzor;
            return String.Format("y={0}x2+{1}x+{2}",a,b,c);
        }

        public List<Double> dajMiejscaZerowe()
        {
            return miejscaZerowe;
        }

    }
}
